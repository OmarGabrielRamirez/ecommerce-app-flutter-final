import 'package:ecom_app/repository/repository.dart';
import 'package:http/http.dart' as http;

class ProductService{

  Repository _repository; 
  ProductService(){
    _repository = Repository();
  }

  getHotProducts() async {
       return await http.get('http://touch-connect.online/api/products?limit=100&page=1&featured=1');
  }

  getAllnewArrivalsProducts() async {
        return await http.get('http://touch-connect.online/api/products?limit=10&page=1');
  }
  getProductsByCategoryId(categoryId) async{
    return await http.get('http://touch-connect.online/api/products?category_id=$categoryId');
  }

  getAllProducts() async {
    return await _repository.httpGet("products");
  }
  
}