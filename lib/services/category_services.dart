import 'package:ecom_app/repository/repository.dart';
import 'package:http/http.dart' as http;

class CategoryService {
  Repository _repository;
  CategoryService() {
    _repository = Repository();
  }

  getCategories() async {
    return await http.get('http://ecommerce.touch-connect.online/api' + '/' + 'categories');
  }
}
