import 'package:ecom_app/models/user_data_drawer.dart';
import 'package:ecom_app/models/user_sesion.dart';
import 'package:ecom_app/screens/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<bool> saveSessionUser(
    UserSession usersesion, BuildContext context) async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  _prefs.setInt('userId', usersesion.idUser);
  _prefs.setString('userName', usersesion.name);
  _prefs.setString('userEmail', usersesion.email);
  _prefs.setString('tokenUser', usersesion.token);

  if (_prefs.getString('userName').isNotEmpty &&
      _prefs.getString('userEmail').isNotEmpty &&
      _prefs.getInt('userId').toString().isNotEmpty &&
      _prefs.getString('tokenUser').isNotEmpty) {
    return true;
  } else {
    return false;
  }
}

Future<void> closeSession(BuildContext context) async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  if (_prefs.getString('tokenUser') == null) {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => LoginScreen(),
      ),
    );
  } else {
    _prefs.clear();
  }
}

Future<UserDataDrawer> getDataUserForMenu() async {
  SharedPreferences _prefs = await SharedPreferences.getInstance();
  UserDataDrawer userDataByDrawer = UserDataDrawer();

  if (_prefs.getString('tokenUser') == null) {
    userDataByDrawer.email = 'invitado@invitado.com.mx';
    userDataByDrawer.name = 'Usuario Invitado';
  } else {
    userDataByDrawer.email = _prefs.getString('userEmail');
    userDataByDrawer.name = _prefs.getString('userName');
  }

  return userDataByDrawer;
}
