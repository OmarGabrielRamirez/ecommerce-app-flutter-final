import 'package:ecom_app/repository/repository.dart';
import 'package:http/http.dart' as http;

class SliderService {
  Repository _repository;
  SliderService() {
    _repository = Repository();
  }

  getSliders() async {
    return await http.get('http://touch-connect.online/api/sliders');
  }
}
