import 'package:ecom_app/models/product.dart';
import 'package:ecom_app/screens/product_detail.dart';
import 'package:flutter/material.dart';

class HomeNewProduct extends StatefulWidget {
  final Product product;

  HomeNewProduct(this.product);

  @override
  _HomeNewProductState createState() => _HomeNewProductState();
}

class _HomeNewProductState extends State<HomeNewProduct> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 190.0,
      height: 260.0,
      child: InkWell(
        onTap: () {
          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (context) =>
                      ProductDetailScreen(this.widget.product)));
        },
        child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
            ),
            elevation: 5,
            margin: EdgeInsets.all(10),
            child: Stack(
              children: <Widget>[
                Image.network(
                  widget.product.photo,
                ),
                Positioned(
                  left: 10.0,
                  top: 10.0,
                  child: Text(
                    this.widget.product.name,
                    style: TextStyle(
                        fontSize: 12,
                        color: Colors.black,
                        fontFamily: 'Lato',
                        // fontWeight: FontWeight.bold,
                        letterSpacing: 1.0),
                  ),
                ),
                Positioned(
                  left: 10.0,
                  bottom: 10.0,
                  child: Text(
                   'Price: \$'+ this.widget.product.price.toStringAsFixed(2),
                    style: TextStyle(
                        fontSize: 12,
                        color: Colors.black,
                        fontFamily: 'Lato',
                        fontWeight: FontWeight.bold,
                        letterSpacing: 1.0),
                  ),
                ),
                Row(
                  children: <Widget>[
                    Text('Price: ${this.widget.product.price}'),
                    Text('Discount: ${this.widget.product.discount}'),
                  ],
                ),
              ],
            )),
      ),
    );
  }
}
