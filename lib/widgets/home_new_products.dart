import 'package:ecom_app/models/product.dart';
import 'package:flutter/material.dart';
import 'home_new_product.dart';

class HomeNewProducts extends StatefulWidget {
  final List<Product> productNewList;
  HomeNewProducts({this.productNewList});

  @override
  _HomeNewProductsState createState() => _HomeNewProductsState();
}

class _HomeNewProductsState extends State<HomeNewProducts> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 205.0,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: this.widget.productNewList.length,
        itemBuilder: (context, index) {
          return HomeNewProduct(this.widget.productNewList[index]);
        },
      ),
    );
  }
}
