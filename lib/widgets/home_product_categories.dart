import 'package:ecom_app/models/category.dart';
import 'package:flutter/material.dart';

import 'home_product_category.dart';

class HomeProductCategories extends StatefulWidget {
  final List<Category> categoryList;
  HomeProductCategories({this.categoryList});

  @override
  _HomeProductCategoriesState createState() => _HomeProductCategoriesState();
}

class _HomeProductCategoriesState extends State<HomeProductCategories> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 205.0,
      child: ListView.builder(
        padding: EdgeInsets.all(0),
        scrollDirection: Axis.horizontal,
        itemCount: this.widget.categoryList.length,
        itemBuilder: (context, index) {
          return HomeProductCategory(this.widget.categoryList[index].url,this.widget.categoryList[index].name,
              this.widget.categoryList[index].id);
        },
      ),
    );
  }
}
