class VariantProduct {
  int id;
  String name;
  double price;

  VariantProduct({this.id, this.name, this.price});
}