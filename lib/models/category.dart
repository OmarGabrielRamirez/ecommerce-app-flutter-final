class Category {
  int id;
  String url;
  String name;

  Category({this.id, this.name, this.url});
}
