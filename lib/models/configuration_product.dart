class CProduct {
  int id;
  String name;
  List<dynamic> options;

  CProduct({this.id, this.name, this.options});
}
