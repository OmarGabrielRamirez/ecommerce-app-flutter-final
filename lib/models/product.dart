class Product {
  int id;
  String name;
  String photo;
  String priceVisual;
  double price;
  int discount;
  String productDetail;
  int quantity;
  List <dynamic> variants;
  List <dynamic> conf;

  toMap(){
    var map = Map<String, dynamic>();
    map['productId'] = id;
    map['productName'] = name;
    map['productPhoto'] = photo;
    map['productPrice'] = price;
    map['productDiscount'] = discount;
    map['productQuantity'] = quantity;
    return map;
  }

  fromMap(){

  }
}