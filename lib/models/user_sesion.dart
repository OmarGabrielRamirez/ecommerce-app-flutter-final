class UserSession {
  int idUser;
  String name;
  String email;
  String token;

  UserSession({this.idUser, this.name, this.email, this.token});
}
