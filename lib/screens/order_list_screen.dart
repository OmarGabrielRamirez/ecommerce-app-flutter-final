import 'dart:convert';

import 'package:ecom_app/models/order.dart';
import 'package:ecom_app/services/order_services.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OrderListScreen extends StatefulWidget {
  OrderListScreen({Key key}) : super(key: key);

  @override
  _OrderListScreenState createState() => _OrderListScreenState();
}

class _OrderListScreenState extends State<OrderListScreen> {
  List<Order> _orderList = List<Order>();

  @override
  void initState() {
    super.initState();
    _getOrderListByUserId();
  }

  _getOrderListByUserId() async {
    SharedPreferences _prefs = await SharedPreferences.getInstance();
    int _userId = _prefs.getInt('userId');
    OrderService _orderService = OrderService();
    var result = await _orderService.getOrderByUserId(_userId);
    var orders = json.decode(result.body);
    orders.forEach((order) {
      var model = Order();
      model.id = order['id'];
      model.quantity = order['quantity'];
      model.amount = double.tryParse(order['amount'].toString());
      model.product.name = order['product']['name'];
      model.product.photo = order['product']['photo'];

      setState(() {
        _orderList.add(model);
      });
    });
    print(result.body);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Order List Screen'),
        ),
        body: ListView(
          children: [
            Card(
                child: ListTile(
              leading: Icon(Icons.shopping_bag_outlined),
              title: Text('Order #1292'),
              subtitle: Text(
                'Total: \$220.00',
              ),
            )),
            Card(
                child: ListTile(
              leading: Icon(Icons.shopping_bag_outlined),
              title: Text('Order #1292'),
              subtitle: Text(
                'Total: \$100.00',
              ),
            )),
            Card(
                child: ListTile(
                    leading: Icon(Icons.shopping_bag_outlined),
                    title: Text('Order #1292'),
                    subtitle: Text(
                      'Total: \$34.00',
                    )))
          ],
        ));
  }
}
