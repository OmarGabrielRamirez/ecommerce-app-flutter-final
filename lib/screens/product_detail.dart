import 'package:ecom_app/models/configuration_product.dart';
import 'package:ecom_app/models/product.dart';
import 'package:ecom_app/models/category.dart';
import 'package:ecom_app/models/variant_product.dart';
import 'package:ecom_app/providers/cart_provider.dart';
import 'package:ecom_app/screens/cart_screen.dart';
import 'package:ecom_app/screens/home_screen.dart';
import 'package:html/parser.dart' show parse;
import 'package:ecom_app/services/cart_services.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ProductDetailScreen extends StatefulWidget {
  final Product product;

  ProductDetailScreen(this.product);
  @override
  _ProductDetailScreenState createState() => _ProductDetailScreenState();
}

class _ProductDetailScreenState extends State<ProductDetailScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  CartService _cartService = CartService();

  List<Product> _cartItems;
  List<CProduct> _configProductList = [];
  List<VariantProduct> _variantProductList = [];

  void initState() {
    super.initState();
    _getConfig();
    _getVariants();
    _getCartItems();
  }

  _getConfig() async {
    List<dynamic> _configProduct = this.widget.product.conf;
    if (_configProduct != null) {
      for (var item in _configProduct) {
        _configProductList.add(
          CProduct(
              id: item['id'], name: item['name'], options: item['options']),
        );
        setState(() {});
      }
    }
  }

  _getVariants() async {
    List<dynamic> _variantsList = this.widget.product.variants;
    if (_variantsList != null) {
      for (var item in _variantsList) {
        _variantProductList.add(
          VariantProduct(
              id: item['id'],
              name: item['name'],
              price: double.parse(item['price'])),
        );
        setState(() {});
      }
    }
  }

  _getCartItems() async {
    _cartItems = List<Product>();
    var cartItems = await _cartService.getCartItems();
    cartItems.forEach((data) {
      var product = Product();
      product.id = data['productId'];
      product.name = data['productName'];
      product.photo = data['productPhoto'];
      product.price = data['productPrice'].toDuble;
      product.discount = data['productDiscount'];
      product.productDetail = data['productDetail'] ?? 'No detail';
      product.quantity = data['productQuantity'];

      setState(() {
        _cartItems.add(product);
      });
    });
  }

  _addToCart(BuildContext context, Product product) async {
    var result = await _cartService.addToCart(product);
    if (result > 0) {
      _getCartItems();
      _showSnackMessage(Text(
        'Item added to cart sucessfully',
        style: TextStyle(color: Colors.green),
      ));
    } else {
      _showSnackMessage(
          Text('Failed to add to cart', style: TextStyle(color: Colors.red)));
    }
  }

  _showSnackMessage(message) {
    var snackBar = SnackBar(
      content: message,
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    final cartProvider = Provider.of<CartProvider>(context);
    cartProvider.cartItems();

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        /* leading: IconButton(icon: Icon(Icons.arrow_back_ios), onPressed: (){
          Navigator.push(context, MaterialPageRoute(builder: (context) => HomeScreen(),),);
        }),*/
        title: Text(this.widget.product.name),
        backgroundColor: Colors.redAccent,
        actions: <Widget>[
          InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => CartScreen(_cartItems)));
            },
            child: Padding(
              padding: const EdgeInsets.all(10.0),
              child: Container(
                height: 150,
                width: 30,
                child: Stack(
                  children: <Widget>[
                    IconButton(
                      iconSize: 30,
                      icon: Icon(
                        Icons.shopping_cart,
                        color: Colors.white,
                      ),
                      onPressed: () {},
                    ),
                    Positioned(
                      child: Stack(
                        children: <Widget>[
                          Icon(Icons.brightness_1,
                              size: 25, color: Colors.black38),
                          Positioned(
                            top: 4.0,
                            right: 8.0,
                            child: Center(
                              child: Text(cartProvider.getCartQuantity.toString()),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
      body: ListView(children: <Widget>[
        Container(
          height: 300,
          child: GridTile(
            child: Padding(
              padding: const EdgeInsets.only(bottom: 60.0),
              child: Container(
                child: widget.product.photo == null
                    ? Center(
                        child: Text('Sin imagen'),
                      )
                    : Image.network(this.widget.product.photo),
              ),
            ),
            footer: Padding(
              padding: const EdgeInsets.only(left: 10.0),
              child: Container(
                child: ListTile(
                  leading: Text(
                    this.widget.product.name,
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold),
                  ),
                  title: Row(children: <Widget>[
                    Expanded(
                      child: Text(
                          '${this.widget.product.price - this.widget.product.discount}',
                          style: TextStyle(
                              color: Colors.redAccent,
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold)),
                    ),
                    Expanded(
                      child: Text('${this.widget.product.price}',
                          style: TextStyle(
                              color: Colors.black54,
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold,
                              decoration: TextDecoration.lineThrough)),
                    ),
                  ]),
                ),
              ),
            ),
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            FlatButton(
              onPressed: () async {
                var result = await cartProvider.addToCart(this.widget.product);
                if (result > 0) {
                  _showSnackMessage(Text(
                    'Item added to cart sucessfully',
                    style: TextStyle(color: Colors.green),
                  ));
                } else {
                  _showSnackMessage(Text('Failed to add to cart',
                      style: TextStyle(color: Colors.red)));
                }

                _addToCart(context, this.widget.product);
              },
              textColor: Colors.redAccent,
              child: Row(
                children: <Widget>[
                  Text('Add to cart'),
                  Icon(Icons.shopping_cart),
                ],
              ),
            ),
            IconButton(
              onPressed: () {},
              icon: Icon(Icons.favorite_border, color: Colors.redAccent),
            ),
          ],
        ),
        Divider(),
        _configProductList.length > 0
            ? Container(
                padding: EdgeInsets.only(left: 30, top: 10, bottom: 10),
                child: Text('Configuracion producto: ',
                    style: TextStyle(fontWeight: FontWeight.bold)),
              )
            : Container(),
        Divider(),
        _configProductList.length > 0
            ? SizedBox(
                height: 120,
                child: Scrollbar(
                  child: ListView.builder(
                      shrinkWrap: true,
                      physics: ClampingScrollPhysics(),
                      padding: EdgeInsets.only(left: 20, right: 20),
                      itemCount: _configProductList.length,
                      itemBuilder: (BuildContext ctxt, int index) {
                        return ExpansionTile(
                          onExpansionChanged: (value) {
                            setState(() {
                              // isExpand = value;
                            });
                          },
                          children: [
                            SizedBox(
                              height: 100,
                              child: ListView.builder(
                                shrinkWrap: true,
                                padding: EdgeInsets.only(left: 20, right: 20),
                                itemCount:
                                    _configProductList[index].options.length,
                                itemBuilder: (BuildContext ctxt, int index2) {
                                  return Container(
                                    child: CheckboxListTile(
                                      value: false,
                                      onChanged: (val) {},
                                      title: Text(
                                        _configProductList[index]
                                            .options[index2]['label'],
                                        style: TextStyle(fontSize: 13),
                                      ),
                                    ),
                                  );
                                },
                              ),
                            )
                          ],
                          trailing: Icon(
                            Icons.arrow_drop_down,
                            size: 32,
                          ),
                          title: Container(
                            width: double.infinity,
                            child: Text(
                              _configProductList[index].name,
                              style: TextStyle(fontSize: 13),
                            ),
                          ),
                        );
                      }),
                ),
              )
            : Container(),
        Divider(),
        _variantProductList.length > 0
            ? Container(
                padding: EdgeInsets.only(left: 30, top: 10, bottom: 10),
                child: Text('Variantes producto: ',
                    style: TextStyle(fontWeight: FontWeight.bold)),
              )
            : Container(),
        Divider(),
        _variantProductList.length > 0
            ? Container(
                child: SizedBox(
                    height: 120,
                    child: Scrollbar(
                        child: ListView.builder(
                            shrinkWrap: true,
                            physics: ClampingScrollPhysics(),
                            padding: EdgeInsets.only(left: 20, right: 20),
                            itemCount: _variantProductList.length,
                            itemBuilder: (BuildContext ctxt, int index) {
                              return Container(
                                child: CheckboxListTile(
                                  value: false,
                                  onChanged: (val) {},
                                  title: Text(
                                    _variantProductList[index].name,
                                    style: TextStyle(fontSize: 12),
                                  ),
                                ),
                              );
                            }))),
              )
            : Container(),
        Padding(
          padding: const EdgeInsets.only(left: 10.0),
          child: ListTile(
            title: Text(
              'Product Detail',
              style: TextStyle(fontSize: 20.0),
            ),
            subtitle: Text(
              this
                  .widget
                  .product
                  .productDetail
                  .replaceAll(RegExp('<p>'), '')
                  .replaceAll(RegExp('</p>'), ''),
            ),
          ),
        )
      ]),
    );
  }
}
