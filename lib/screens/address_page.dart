import 'package:ecom_app/screens/add_address.dart';
import 'package:flutter/material.dart';

class AddressPage extends StatefulWidget {
  @override
  _AddressPageState createState() => _AddressPageState();
}

class _AddressPageState extends State<AddressPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
              icon: Icon(
                Icons.add,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => AddAddresPage(),
                  ),
                );
              })
        ],
        title: Text('Address'),
      ),
      body: SafeArea(
          child: Container(
        child: ListView(
          padding: EdgeInsets.all(10),
          children: [
            Card(
              elevation: 3,
              child: ListTile(
                leading: Icon(Icons.place),
                title: Text('Address, City. Contry. CP.22222'),
                subtitle: Text('5567891234'),
              ),
            ),
            Card(
              elevation: 3,
              child: ListTile(
                leading: Icon(Icons.place),
                title: Text('Address, City. Contry. CP.22222'),
                subtitle: Text('5567891234'),
              ),
            )
          ],
        ),
      )),
    );
  }
}
