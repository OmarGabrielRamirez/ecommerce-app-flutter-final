import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AddAddresPage extends StatefulWidget {
  AddAddresPage();

  @override
  _AddAddresPageState createState() => _AddAddresPageState();
}

class _AddAddresPageState extends State<AddAddresPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  _showSnackMessage(message) {
    var snackBar = SnackBar(
      content: message,
    );
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  _saveAddress(String address, String city, String postCode, String state,
      String phone) async {
    if (address.isEmpty || city.isEmpty || postCode.isEmpty || state.isEmpty) {
      _showSnackMessage(
        Text(
          'Add new address error!',
          style: TextStyle(color: Colors.red),
        ),
      );
    } else {
      SharedPreferences _prefs = await SharedPreferences.getInstance();
      String token = _prefs.getString('tokenUser');
      var responseAddAddress = await http.post(
        "http://touch-connect.online/api/addresses/create?token=true",
        headers: <String, String>{
          'Content-Type': 'application/json',
          'Authorization': 'Bearer token-$token',
        },
      );
      print(responseAddAddress.statusCode);
      if (responseAddAddress.statusCode == 200) {
      } else {
        _showSnackMessage(
          Text(
            'Add new address error!',
            style: TextStyle(color: Colors.red),
          ),
        );
      }
    }
  }

  final address = TextEditingController();
  final city = TextEditingController();
  final postcode = TextEditingController();
  final state = TextEditingController();
  final phone = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Add address'),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 0.0),
        child: ListView(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(
                  left: 28.0, top: 28.0, right: 28.0, bottom: 14.0),
              child: Text('Add Shipping Address',
                  style:
                      TextStyle(fontSize: 28.0, fontWeight: FontWeight.bold)),
            ),
            Divider(
              height: 5.0,
              color: Colors.black,
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 28.0, top: 14.0, right: 28.0, bottom: 14.0),
              child: TextField(
                controller: address,
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                    hintText: 'Enter your address',
                    labelText: 'Enter your address'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 28.0, top: 14.0, right: 28.0, bottom: 14.0),
              child: TextField(
                keyboardType: TextInputType.text,
                controller: city,
                decoration: InputDecoration(
                    hintText: 'Enter your city', labelText: 'Enter your city'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 28.0, top: 14.0, right: 28.0, bottom: 14.0),
              child: TextField(
                keyboardType: TextInputType.number,
                controller: postcode,
                decoration: InputDecoration(
                    hintText: 'Enter your postcode',
                    labelText: 'Enter your postcode'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 28.0, top: 14.0, right: 28.0, bottom: 14.0),
              child: TextField(
                keyboardType: TextInputType.text,
                controller: state,
                decoration: InputDecoration(
                    hintText: 'Enter your state',
                    labelText: 'Enter your state'),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: 28.0, top: 14.0, right: 28.0, bottom: 14.0),
              child: TextField(
                keyboardType: TextInputType.number,
                controller: phone,
                decoration: InputDecoration(
                    hintText: 'Enter your phone',
                    labelText: 'Enter your phone'),
              ),
            ),
            Column(
              children: <Widget>[
                ButtonTheme(
                  minWidth: 320.0,
                  height: 45.0,
                  child: FlatButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(7.0)),
                    color: Colors.redAccent,
                    onPressed: () {
                      _saveAddress(address.text, city.text, postcode.text,
                          state.text, phone.text);
                    },
                    child: Text('Add Address',
                        style: TextStyle(color: Colors.white)),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
